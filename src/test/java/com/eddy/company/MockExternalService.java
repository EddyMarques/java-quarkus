package com.eddy.company;

import io.quarkus.test.Mock;

import javax.enterprise.context.ApplicationScoped;

@Mock
@ApplicationScoped
public class MockExternalService extends ExternalService{
    @Override
    public String service() {
        return "mock";
    }
}
