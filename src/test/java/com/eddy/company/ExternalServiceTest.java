package com.eddy.company;

import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;

@QuarkusTest
class ExternalServiceTest {

    @Inject
    private ExternalService service;

    @Test
    void service() {
        System.out.println(service.service());
        Assertions.assertEquals("mock", service.service());
    }
}