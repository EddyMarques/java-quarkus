package com.eddy.company.dto;

import java.util.List;

public class BookDTO {

    private String id;
    private String title;
    private String subTitle;
    private List<String> authors;

}
