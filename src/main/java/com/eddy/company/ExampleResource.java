package com.eddy.company;

import com.eddy.company.dto.BookDTO;
import com.eddy.company.service.BookService;
import org.eclipse.microprofile.rest.client.inject.RestClient;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.Set;

@Path("/hello")
public class ExampleResource {

    @Inject
    @RestClient
    BookService bookService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Set<BookDTO> getAll() {
        System.out.println(bookService.getAll());
        return bookService.getAll();
    }

}