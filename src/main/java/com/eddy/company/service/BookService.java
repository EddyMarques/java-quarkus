package com.eddy.company.service;

import com.eddy.company.dto.BookDTO;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.util.Set;

@Path("/v1")
@RegisterRestClient
public interface BookService {

    @GET
    @Path("books/volumes?q=flowers+inauthor:keyes&startIndex=0&maxResults=1")
    @Produces("application/json")
    Set<BookDTO> getAll();
}
